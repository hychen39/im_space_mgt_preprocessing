#!/usr/bin/perl
use strict;
use warnings;
use Encode;
# use diagnostics;
# use open IN => ":encoding(utf8)", OUT => ":utf8"; # See http://perldoc.perl.org/open.html

use File::Log; # See http://search.cpan.org/~gng/File-Log-1.05/Log.pm

 
# require 'D:\\0work1\\Projects\\106-im_Space_mgt\\subprojects\\course_import_preprocess\\clean_data_lib.pm';
require './clean_data_lib.pm';

# Pretty format, all the parameters
 my $log = File::Log->new({
   debug           => 4,                   # Set the debug level
   logFileName     => 'preprocess.log',     # define the log filename
   logFileMode     => '>',                 # '>>' Append or '>' overwrite
   dateTimeStamp   => 1,                   # Timestamp log data entries
   stderrRedirect  => 1,                   # Redirect STDERR to the log file
   defaultFile     => 0,                   # Use the log file as the default filehandle
   logFileDateTime => 0,                   # Timestamp the log filename
   appName         => 'course_import_preprocess', # The name of the application
   PIDstamp        => 1,                   # Stamp the log data with the Process ID
   storeExpText    => 1,                   # Store internally all exp text
   msgprepend      => '',                  # Text to prepend to each message
   say             => 1,                   # msg() and exp() methode act like the perl6 say
                                           #  command (default off) requested by Aaleem Jiwa
                                           #  however it might be better to just use the say()
                                           #  method
 });

# binmode(STDOUT, ":utf8"); ## output utf8 
my $line='';
my $result='';

if (@_ < 2) {
  print STDOUT "Please provide input and output filenames \n";
  print STDOUT "Usage: \n";
  print STDOUT "\tperl course_import_preproces input_file output_file \n";
  exit;
}

my $input_filename = shift;  # arg1: input filename
# my $input_filename = "106-1course.csv";  # arg1: input filename
my $output_filename = shift; # arg2: output filename
# my $output_filename = "out.csv"; # arg2: output filename
open INPUT_FH, "<", $input_filename;
open OUTPUT_FH, ">", $output_filename;

# print column title
print OUTPUT_FH "course_info\tcourse_days\tcourse_csession\tcourse_room\n";

# print rows
while(defined($line=<INPUT_FH>)){
  chomp $line;
  my @course_line;
  @course_line = &parse_line($line);
  # print "Parse Reslut: 1)$course_line[0]  2)$course_line[1]  3)$course_line[2] \n";
  my $course_info = &convert_course_info($course_line[0]);
  my $course_time_exp = &expand_course_time_exp($course_line[1]);    
  my $course_room = $course_line[2];
  my $c_length = length($course_info);
  # print "Length: $c_length";
  #  print OUTPUT_FH  "Length: $c_length \n";
  if (length($course_info) <= 1){
    $log->msg(4, "Bad Row: $line. Cannot parse it.");
  } else {
    $result = $course_info."\t".$course_time_exp."\t".$course_room;
    # $result = decode('utf8',  $course_info.";".$course_time_exp.";".$course_room);
    # printf "Before: $line, After:%s;%s;%s \n", $course_info, $course_time_exp, $course_room;
    print OUTPUT_FH $result."\n";
  }
}

$log->close();
close INPUT_FH;
close OUTPUT_FH;