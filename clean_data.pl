#!/usr/bin/perl
use strict;

# use lib 'd:\\0work1\\Projects\\106-im_Space_mgt\\src';
# use clean_data_lib ":ALL:";
# require 'd:\\0work1\\Projects\\106-im_Space_mgt\\src\\clean_data_lib.pm';
require 'D:\\0work1\\Projects\\106-im_Space_mgt\\subprojects\\course_import_preprocess\\clean_data_lib.pm';
# ToDo:Read from file



# Test parsing a line

my $line = '7426,研究方法,資管博日一A,陳怜秀,(二),"9,A,中午",M-309';
my @course_line = &parse_line($line);
print $course_line[0]."\n";
print $course_line[1]."\n";
print $course_line[2]."\n";
print &convert_course_info($course_line[0])."\n";
print &expand_course_time_exp($course_line[1])."\n";
print $course_line[2]."\n";


 my $line = "7443,高等電子商務,資管碩日一A,薛夙珍,(三)、(四),5-6、8,M-212";
 my @test_data;
 push(@test_data, '7426,研究方法,資管博日一A,陳怜秀,(二),"9,A,中午",M-309');
 push(@test_data, "7443,高等電子商務,資管碩日一A,薛夙珍,(三)、(四),5-6、8,M-212");
 push(@test_data, '2510,Web程式設計,資管四日資管組二B,唐元亮,(三)、(五),"5,中午、4",M-516');
 push(@test_data, '3531,產業實習(一),資管四進二A,洪朝貴,(六), 9-B,');
 push(@test_data,'7784,碩士論文,資管碩在二A,呂慈純,,,');
 push(@test_data,'2513,資料結構,資管四日資管組二C,戴紹國,(四)、(五),8、2-3,T2-406');
 

for (@test_data){
  @course_line = parse_line($_); 
  my $course_info = &convert_course_info($course_line[0]);
  my $course_time_exp = &expand_course_time_exp($course_line[1]);    
  my $course_room = $course_line[2];
  printf "Before: $_  After: %s;%s;%s \n", $course_info, $course_time_exp, $course_room;
}
 return;

# Read lines from file
print "Test: lines from actual files------\n";
# my $filename = "D:\\0work1\\Projects\\106-im_Space_mgt\\subprojects\\course_import_preprocess\\106-1course-utf8.txt";
my $filename = "D:\\0work1\\Projects\\106-im_Space_mgt\\subprojects\\course_import_preprocess\\106-1course.csv";

if (!open COURSE_FILE, "<$filename"){
    die "File $filename does not exist.";
}

while(<COURSE_FILE>){
  chomp;
  my @course_line;
#   if (/\".+\"/){
    @course_line = &parse_line($_);
#   } else {
      s/、/,/g; #
#   }
  my $course_info = &convert_course_info($course_line[0]);
  my $course_time_exp = &expand_course_time_exp($course_line[1]);    
  my $course_room = $course_line[2];
  printf "Before: $_  After: %s;%s;%s \n", $course_info, $course_time_exp, $course_room;
}

close COURSE_FILE;
