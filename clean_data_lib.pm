# package clean_data_lib;
# perldoc ref: https://www.perlmonks.org/?node_id=67783

sub big5_to_utf8 {
    my $data = shift;
    $data = decode('cp950', $data);
    return encode('utf8', $data);
}

=item parse_line()
將輸入的字串分成 3 部份: 課程資訊($course), 上課週天($course_day), 及上課教室($course_room) 
 
 Arguments:
    $line{string}: 包含以下欄位的字串(逗號分隔): 課號,課程名稱,開課單位,授課老師,星期,節次,地點

 Returns: {list} ($course, $course_day, $course_room)
=cut
sub parse_line{
$data = shift;
# my @column = split /,"|",/, $_[0];
$data=~/(.+\)),("?.+"?),(.+)?/;
# $data=~/(.+?,){4}((\(.+\)),("?.+"?),(.+)|,{2})/;
my $course = $1;
my $course_day = $2;
my $course_room=$3;
if (!$course_room){
    $course_room = "N/A";
}
# print "Room:$3,$course_room\n";
# Return value.
($course, $course_day, $course_room)
}

=item decode_chinese_week_day()
將中文的星期轉成阿拉伯數字編碼
# decoding (二) to 2
#print(&decode_chinese_week_day("(二)"));
#print(&decode_chinese_week_day("(三)"));
=cut 
sub decode_chinese_week_day {
# use Switch;
use Encode;

my $data = $_[0];
# print "Before encoding: $data \n";
# $data = decode('cp950', $data); #decide from cp950 (big5) 
# $data = encode('utf8', $data);  # encode to utf8 for comparison.
my $result = 0;
# print "decode_chinese_week_day: data: $data \n ";
if ($data eq "(一)"){
    $result = 1;
} elsif ($data eq "(二)") {
    $result = 2;    
} elsif ($data eq "(三)") {
    $result = 3;    
} elsif ($data eq "(四)") {
    $result = 4;
} elsif ($data eq "(五)") {
    $result = 5;
} elsif ($data eq "(六)") {
    $result = 6;
} elsif ($data eq "(日)") {
    $result = 7;
} else {
    $result = 0;
}
# switch ($data) {
#     case  "(一)" {$result = 1}
#     case  "(二)" {$result = 2}
#     case  "(三)" {$result = 3}
#     case  "(四)" {$result = 4}
#     case  "(五)" {$result = 5}
#     case  "(六)" {$result = 6}
#     case  "(七)" {$result = 7}
#     else {$result = 0}
# }
#Return
$result;
}

=item convert_course_info()
 Conver course info:
 Before conversion: 7426,研究方法,資管博日一A,陳怜秀,(二)、(三)
 After: 7426,研究方法,資管博日一A,陳怜秀;2/3
 
 return {String}:
   轉換後的字串: 課程資訊;上課星期
      
=cut
sub convert_course_info {
    my $course_info = $_[0];
    my $split_pos = index $course_info, ",(";
    my $part1 = substr $course_info, 0, $split_pos;
    my $part2 = substr $course_info, $split_pos+1, length($course_info) - $split_pos;
    # $part2 = decode('cp950', $part2);
    # $part2 = encode('utf8', $part2);
    $part2 = big5_to_utf8($part2);
    $part2 =~ s/、/\//;
    my @days = split /\//, $part2;
    # print "Days @days \n";
    my $day_exp="";
    for (@days){
       $day_exp .= &decode_chinese_week_day($_);
       $day_exp.="/";
    } 
    $day_exp = substr($day_exp, 0, length($day_exp)-1);
    # $course_info =~ /(.+,)(\(.+\))/;
    # $2=~ s/^\s+|\s+$//g; ## trim space
    
    return $part1."\t".$day_exp;
}

=item decode_time_block() 
把 A,B,C... 轉成 10, 11, 12...
=cut
sub decode_time_block {
    my $data = $_[0];
    # print "data: $data";
    my $result;
    if ($data >=1 && $data <=9){
       $result = $data; # return value
    } elsif (length($data) > 1){
        ## 中午
        $result = 15;        
    }
    elsif ($data eq 'A'){
        $result = 10;
    } elsif ($data eq 'B'){
        $result = 11;
    } elsif ($data eq 'C'){
        $result = 12;
    } elsif ($data eq 'D'){
        $result = 13;
    } elsif ($data eq 'E'){
        $result = 14;
    } else {
        # exception
        $result = 0;
    }
    #return
    #print "Result $result";
    return $result;
}

=item expand_time_rang_exp()
Expand the time-range short expression to full expression. For example: '1-3' is expanded to 1:2:3.

The subroute is used by expand_course_time_exp().
=cut

sub expand_time_rang_exp {
    my $data = $_[0];
    my @rang = split /-/, $data;
    my $start = &decode_time_block(@rang[0]);  
    my $end = &decode_time_block(@rang[1]);    
    my $result_time="";
    for (my $i = $start; $i <= $end; $i++){
        $result_time.=$i;
        if ($i < $end) {$result_time.=":"};
    }
    #return 
   $result_time;
}

=item expand_course_time_exp
"9,A,中午" Should convert to 9:10:15
"5-6,中午" Should convert to 5:6:15
"5-7" Should convert to 5:6:7
"5-6、8" Should convert 5:6/8 
=cut
sub expand_course_time_exp {
    my $data = $_[0];
    # print "expand_course_time_exp $data. \n";
    $data =~ s/"//g;  # clean the double quote symbol
    # print "expand_course_time_exp: data: $data \n";
    $data = big5_to_utf8($data);
    $data =~ s/、/\//;
    my @time_blocks_days = split /\//,$data;
    my $result="";
    for (@time_blocks_days){
        my @time_blocks_a_day = split /,/,$_;
        for (@time_blocks_a_day){
          if(/\w-\w/){
              $result.=&expand_time_rang_exp($_);
          } else {
              $result.=&decode_time_block($_);
          }
          $result.=":";
        }
       $result = substr($result, 0, length($result)-1);
       $result.="/";
    }
    $result = substr($result, 0, length($result)-1);
}

1;