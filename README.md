# 課程資料匯入的前處理 perl 程式

在將課程資料匯入 Apex App 前, 使用此程式將原始資料轉換成規範的資料格式.

## 程式安裝
1. 請先安裝 Perl 
2. 下載檔案, 並放在同一個目錄:
    - `course_import_preprocess.pl`: 主要轉換程式
    - `clean_data_lib.pm`: perl subroutines
3. 測試資料檔案: `106-1course.csv`

[執行程式](https://bitbucket.org/hychen39/im_space_mgt_preprocessing/wiki/Home) 

[Perl 執行環境安裝](https://bitbucket.org/hychen39/im_space_mgt_preprocessing/wiki/install_perl_env)

[轉換前的原始資料](https://bitbucket.org/hychen39/im_space_mgt_preprocessing/wiki/course_raw_format)

[轉換後的格式](https://bitbucket.org/hychen39/im_space_mgt_preprocessing/wiki/course_output_format)